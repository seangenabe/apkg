export interface Card {
  /** Card ID. Usually the creation time of the card in Unix milliseconds */
  id: number
  /**
   * Note ID
   */
  nid: number
  /**
   * Deck ID
   */
  did: number
  /**
   * Card template as ordinal of the card template
   * @default 0
   */
  ord: number
  /**
   * Last modified time as Unix seconds
   */
  mod: number
  /**
   * Update sequence number
   */
  usn: number
  /**
   * Type of the card
   */
  type: CardType
  queue: CardQueueStatus
  /**
   * Depending on the type:
   * * new - note ID or random integer
   * * due - integer day, relative to the collection's creation time
   * * learning: timestamp
   */
  due: number
  /**
   * SRS interval, seconds if negative, days if positive
   * @default 0
   */
  ivl: number
  /**
   * Ease factor / 1000
   * @default 2500
   */
  factor: number
  /**
   * Number of reviews
   * @default 0
   */
  reps: number
  /**
   * Number of times this card was answered incorrectly
   * @default 0
   */
  lapses: number
  /**
   * A number representing the number of reps left until graduation
   * @description A number of the form a * 1000 + b where:
   * * a - number of reps left until graduation
   * * b - number of reps left today
   * @default 0
   */
  left: number
  /**
   * original due
   * @default 0
   */
  odue: number
  /**
   * original did
   * @default 0
   */
  odid: number
  /**
   * @default 0
   */
  flags: number
  /**
   * Unused
   * @default 0
   */
  data: string
}

export enum CardType {
  New = 0,
  Learning = 1,
  Due = 2,
  Filtered = 3
}

export enum CardQueueStatus {
  ScheduleBuried = -3,
  UserBuried = -2,
  Suspended = -1,
  New = 0,
  Learning = 1,
  Due = 2,
  ForReview = 3
}
