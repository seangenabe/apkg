export interface CollectionConfiguration {
  /** The ID of the last selected deck */
  curDeck: number
  /** The ID of the last selected deck and its descendants */
  activeDecks: number[]
  /**
   * The mode of studying new cards and reviews
   * @default NewSpread.Distribute
   * */
  newSpread: NewSpread
  /** Learn ahead limit in seconds (preference value * 60)
   * @description If there are no reviews remaining, review ahead cards that will appear in the future within this window.
   * @default 1200
   */
  collapseTime: number
  /**
   * Timebox time limit in seconds (preference value * 60)
   * @default 0
   */
  timeLim: number
  /**
   * Show next review time above answer buttons
   * @default true
   */
  estTimes: boolean
  /**
   * Show remaining card count during review
   * @default tru
   */
  dueCounts: boolean
  /**
   * ID of the last modified model
   */
  curModel: string
  /**
   * When a new card is due
   * @default 1
   * */
  nextPos: number
  /**
   * Current field sorted in the browser
   * @default "noteFld"
   */
  sortType: string
  /**
   * Whether to sort descending
   * @default false
   */
  sortBackwards: boolean
  /**
   * True for "when adding, default to current deck"
   * @default true
   */
  addToCur: boolean
  /**
   * Whether to show new cards with large steps before reviews. Not shown in the GUI.
   * @default false
   */
  dayLearnFirst: boolean
  /**
   * @default true
   */
  newBury: boolean
}

export enum NewSpread {
  Distribute = 0,
  Last = 1,
  First = 2
}
