export interface Collection {
  /**
   * ID of the collection. There's only ever one collection in each profile so it doesn't matter.
   * @default 1
   */
  id: number
  /**
   * Creation time in Unix seconds
   */
  crt: number
  /**
   * Last modified time in Unix milliseconds
   */
  mod: number
  /**
   * Schema modified time
   * @description If the scm of the server and client are different, a full sync would be required
   */
  scm: number
  ver: number // 11
  /**
   * Dirty. Unused
   * @default 0
   */
  dty: number
  /**
   * Update sequence number
   */
  usn: number
  /**
   * Last time synced
   * @default 0
   */
  ls: number
  /**
   * Sync collection configuration as JSON string
   */
  conf: string
  /**
   * Models (note types) as JSON string: ID to model object
   */
  models: string
  /**
   * Decks as JSON string: ID to deck object
   */
  decks: string
  /**
   * Deck option group as JSON string: ID to option group object
   */
  dconf: string
  /**
   * (JSON string)
   * @default {}
   */
  tags: string
}
