/**
 * Deck option group
 */
export interface DeckConfiguration {
  /**
   * Deck configuration ID. This is the same as the deck configuration key in col dconf. Usually in Unix milliseconds but the default option group is 1.
   */
  id: number
  /** The name of the option group */
  name: string
  /** Last modified time in Unix seconds */
  mod: number
  /** Automatically play audio */
  autoplay: boolean
  /** Dynamic deck */
  dyn?: boolean | number
  /** Whether to show timer */
  timer: number
  /** Seconds to run timer */
  maxTaken: number
  /** Whether to replay question audio */
  replayq: boolean
  /** Update sequence number  */
  usn: number
  new: {
    /** Delays in minutes for new cards */
    delays: number[]
    /** Initial ease factor */
    initialFactor: number
    /** Whether to bury cards related to new cards answered */
    bury: boolean
    /**
     * Graduation intervals in days.
     */
    ints: [number, number, number?]
    /**
     * Order in which new cards are shown
     */
    order: NewCardsOrder
    /**
     * Maximum number of new cards shown per day
     */
    perDay: number
    /** Unused
     * @default true
     */
    separate: boolean
  }
  lapse: {
    /** Delays in minutes for lapsed cards */
    delays: number[]
    /** Interval multiplier for lapsed cards (Percent / 100) 0 = reset, 1 = leave interval unchanged */
    mult: number
    /** Minimum interval in days for lapsed cards */
    minInt: number
    /** Number of lapses before marking as leech */
    leechFails: number
    /** Action to take for leeched cards */
    leechAction: LeechAction
  }
  rev: {
    /**
     * Maximum interval for reviews
     */
    maxIvl: number
    /** Whether to bury cards related to new cards answered */
    bury: boolean
    /** Ease */
    ease4: number
    /** Interval randomization (so you don't get reviews in order). Must be near 0 */
    fuzz: number
    /** Maximum number of cards to review per day */
    perDay: number
    /**
     * Hard multiplier (must be near 1)
     */
    hardFactor: number
    /**
     * Global interval multiplier
     */
    ivlFct: number
    /**
     * Unused
     * @default 1
     */
    minSpace: number
  }
}

export enum LeechAction {
  Suspend = 0,
  Mark = 1
}

export enum NewCardsOrder {
  Random = 0,
  Due = 1
}
