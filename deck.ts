export interface Deck {
  /**
   * Deck ID. This is the same as the deck key in the col decks field. Usually in Unix milliseconds.
   */
  id: number
  /**
   * Name of the decks. Nested decks are delimited by ::
   */
  name: string
  /**
   * Last modified time in Unix seconds
   */
  mod: number
  /**
   * Description
   * @default ""
   */
  desc: string
  /** Update sequence number */
  usn: number
  collapsed: boolean
  browserCollapsed?: boolean
  newToday: [number, number]
  revToday: [number, number]
  lrnToday: [number, number]
  timeToday: [number, number]
  /**
   * 1 if dynamic (filtered) deck
   * @default 0
   */
  dyn: 0 | 1
  /**
   * Extended review card limit (for custom study) user setting
   * @default 10
   */
  extendRev?: number
  /**
   * Extended new card limit (for custom study) user setting
   * @default 10
   */
  extendNew?: number
  /**
   * Option group being followed by this deck. (col dconf ID)
   */
  conf?: number
  /** Model ID */
  mid?: string
}
