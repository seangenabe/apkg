import { Card } from "./card"
import { Collection } from "./col"
import { CollectionConfiguration } from "./col-conf"
import { Deck } from "./deck"
import { DeckConfiguration } from "./dconf"
import { initDbSql } from "./init-db"
import { Model } from "./model"
import { Note } from "./note"
import { addColons, fromSqliteRow } from "./util"
import { SqlJs } from "sql.js/module"
import initSqlJs from "sql.js"

export class Apkg {
  media: ReadableFile[] = []
  col: Collection = {
    id: 1,
    crt: 0,
    mod: 0,
    scm: 0,
    ver: 11,
    dty: 0,
    usn: 0,
    ls: 0,
    conf: "{}",
    models: "{}",
    decks: "{}",
    dconf: "{}",
    tags: "{}"
  }
  conf: CollectionConfiguration = {
    nextPos: 1,
    activeDecks: [],
    curDeck: 1,
    estTimes: true,
    sortType: "nodeFld",
    timeLim: 0,
    sortBackwards: false,
    addToCur: true,
    newBury: true,
    newSpread: 0,
    dueCounts: true,
    curModel: "0",
    collapseTime: 1200,
    dayLearnFirst: false
  }
  models: Model[] = []
  decks: Deck[] = []
  dconf: DeckConfiguration[] = []
  notes: Note[] = []
  cards: Card[] = []

  loadFromDb(db: SqlJs.Database) {
    // Load col
    this.col = (db.exec(`SELECT * FROM col`)[0] as unknown) as Collection

    this.conf = JSON.parse(this.col.conf)
    this.models = Object.values(JSON.parse(this.col.models))
    this.decks = Object.values(JSON.parse(this.col.decks))
    this.dconf = Object.values(JSON.parse(this.col.dconf))

    // Load notes
    this.notes = db.exec(`SELECT * FROM notes`).map(fromSqliteRow) as any

    // Load cards
    this.cards = db.exec(`SELECT * FROM cards`).map(fromSqliteRow) as any
  }

  /**
   * Save current state to `this.db`
   */
  saveToDb(db: SqlJs.Database) {
    runInTransaction(db, () => this._saveToDb(db))
  }

  private _saveToDb(db: SqlJs.Database) {
    const decksMap = toMap(this.decks)
    const modelsMap = toMap(this.models)
    const dconfMap = toMap(this.dconf)
    const notesMap = toMap(this.notes)
    // Validate decks
    for (let deck of decksMap.values()) {
      if (deck.conf && !dconfMap.has(deck.conf)) {
        throw new Error(
          `dconf ${deck.conf} for deck ${deck.id} (${deck.name}) not found`
        )
      }
    }
    // Validate conf
    if (!decksMap.has(this.conf.curDeck)) {
      throw new Error(`curDeck ${this.conf.curDeck} not found`)
    }
    for (let deckID of this.conf.activeDecks) {
      if (!decksMap.has(deckID)) {
        throw new Error(`deck with ID ${deckID} in activeDecks not found`)
      }
    }
    // Validate notes
    for (let note of this.notes) {
      if (!modelsMap.has(note.mid)) {
        throw new Error(`Model ID ${note.mid} not found on note ${note.id}`)
      }
    }
    // Validate cards
    for (let card of this.cards) {
      if (!notesMap.has(card.nid)) {
        throw new Error(`Note ID ${card.nid} not found on card ${card.id}`)
      }
      if (!decksMap.has(card.did)) {
        throw new Error(`Deck ID ${card.did} not found on card ${card.did}`)
      }
    }
    // Validate models
    for (let model of this.models) {
      const fieldOrds = new Set<number>()
      for (let field of model.flds) {
        if (fieldOrds.has(field.ord)) {
          throw new Error(
            `Model with ID ${model.id} has duplicated field ord ${field.ord}`
          )
        } else {
          fieldOrds.add(field.ord)
        }
      }
    }

    this.col.conf = str(this.conf)
    this.col.dconf = str(fromEntries(dconfMap))
    this.col.decks = str(fromEntries(decksMap))
    this.col.models = str(fromEntries(modelsMap))
    db.exec(`DELETE FROM col`)
    db.prepare(
      `INSERT INTO col VALUES(${"id crt mod scm ver dty usn ls conf models decks dconf tags"
        .split(" ")
        .map(f => `:${f}`)
        .join(",")})`
    ).run(addColons((this.col as unknown) as SqlJs.ParamsObject))

    this.saveNotesToDb(db)
    this.saveCardsToDb(db)
  }

  private saveNotesToDb(db: SqlJs.Database) {
    db.exec(`DELETE FROM notes`)
    const statement = db.prepare(
      `INSERT INTO notes VALUES(${"id guid mid mod usn tags flds sfld csum flags data"
        .split(" ")
        .map(f => `:${f}`)
        .join(",")})`
    )
    for (let note of this.notes) {
      statement.run(addColons((note as unknown) as SqlJs.ParamsObject))
    }
  }

  private saveCardsToDb(db: SqlJs.Database) {
    db.exec(`DELETE FROM cards`)
    const statement = db.prepare(
      `INSERT INTO cards VALUES(${"id nid did ord mod usn type queue due ivl factor reps lapses left odue odid flags data"
        .split(" ")
        .map(f => `:${f}`)
        .join(",")})`
    )
    for (let card of this.cards) {
      statement.run(addColons((card as unknown) as SqlJs.ParamsObject))
    }
  }

  async load(files: IterableIterator<ReadableFile>) {
    let mediaJson: Record<number, string> = {}
    const mediaMap = new Map<number, ReadableFile>()
    for (let file of files) {
      if (file.name === "collection.anki2") {
        const SQL = await initSqlJs()
        const db = new SQL.Database(await readAllFromFile(file))
        this.loadFromDb(db)
      } else if (file.name === "media") {
        mediaJson = JSON.parse(
          Buffer.from(await readAllFromFile(file)).toString("utf8")
        )
      } else {
        const result = file.name.match(/^(\d+)$/)
        if (result) {
          mediaMap.set(Number(result[0]), file)
        }
      }
    }
    const media: ReadableFile[] = []
    for (let [num, filename] of Object.entries(mediaJson)) {
      const file = mediaMap.get(Number(num))
      if (file) {
        media.push({
          name: filename,
          read: () => file.read()
        })
      }
    }
    this.media = media
  }

  /**
   * Iterates through all of the intended contents of the apkg zip file.
   */
  *save(): IterableIterator<ReadableFile> {
    const t = this
    yield {
      name: "collection.anki2",
      async *read() {
        const SQL = await initSqlJs()
        const db = new SQL.Database()
        db.run(initDbSql())
        t.saveToDb(db)
        yield db.export()
      }
    }

    let i = 0
    const mediaJson: Record<number, string> = {}
    for (let media of this.media) {
      mediaJson[i] = media.name
      yield {
        name: `${i}`,
        read: () => media.read()
      }
      i++
    }

    yield {
      name: "media",
      async *read() {
        yield Buffer.from(JSON.stringify(mediaJson))
      }
    }
  }
}

export interface File {
  name: string
}

export interface ReadableFile extends File {
  read(): AsyncIterableIterator<Uint8Array>
}

const str = (value: any) => JSON.stringify(value)

function toMap<T extends { id: number }>(values: T[]) {
  const map = new Map<number, T>()
  for (let value of values) {
    if (map.has(value.id)) {
      throw new Error(`ID ${value.id} duplicated`)
    }
    map.set(value.id, value)
  }
  return map
}

function fromEntries<V>(
  entries: Iterable<[string | number, V]>
): { [key in string | number]: V } {
  const ret: { [key in string | number]: V } = {}
  for (let [key, value] of entries) {
    ret[key] = value
  }
  return ret
}

async function readAllFromFile(file: ReadableFile): Promise<Uint8Array> {
  const chunks: Uint8Array[] = []
  let length = 0
  for await (let chunk of file.read()) {
    chunks.push(chunk)
    length += chunk.length
  }
  const ret = new Uint8Array(length)
  let offset = 0
  for (let chunk of chunks) {
    ret.set(chunk, offset)
    offset += chunk.length
  }
  return ret
}

function runInTransaction(db: SqlJs.Database, fn: () => void) {
  try {
    db.run("BEGIN")
    fn()
    db.run("COMMIT")
  } catch (err) {
    db.run("ROLLBACK")
    throw err
  }
}
