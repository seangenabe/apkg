/**
 * Model (note type)
 */
export interface Model {
  /** Model ID. This is the same as the key of the model in the col model field. Usually the model creation time in Unix milliseconds. */
  id: number
  /** Deck ID where new cards are added to */
  did: number
  /** Model name */
  name: string
  /** Last modified time in Unix seconds */
  mod: number
  /**
   * CSS, shared by all templates
   */
  css: string
  /**
   * Templates followed by cards
   */
  tmpls: CardTemplate[]
  /** Fields in the model */
  flds: ModelField[]
  /**
   * Which fields are required to generate each template.
   * @description
   * Each tuple corresponds to a template and has the elements:
   * * 0 - the ordinal of the template
   * * 1
   *   * If "none", do not generate cards for this template. Element 2 should be empty.
   *   * If "all", a card is generated if all fields of element 2 are filled
   *   * If "any", a card is generated if any field of element 2 is filled
   * * 2 - Ordinals of fields
   *
   * https://github.com/Arthur-Milchior/anki/blob/master/documentation/templates_generation_rules.md
   * */
  req: [number, "none" | "all" | "any", number[]][]
  /** Update sequence number */
  usn: number
  /**
   * String inserted at the beginning of LaTeX expressions
   * @default "\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage[utf8]{inputenc}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n"
   */
  latexPre: string
  /**
   * String added to the end of LaTeX expressions
   * @default "\\end{document}"
   * */
  latexPost: string
  /**
   * Which field to sort with (use the field ordinal)
   * @default 0
   */
  sortf: number
  /**
   * Tags of the last added note
   * @default []
   */
  tags: string[]
  /**
   * Legacy version number. Must be an empty array.
   * @default []
   */
  vers: []
  prewrap?: boolean
  latexsvg?: boolean
  type?: number
}

/**
 * Template followed by a set of cards
 */
export interface CardTemplate {
  /** Card template name */
  name: string
  /** Question format */
  qfmt: string
  /** Answer format */
  afmt: string
  /** Question format for browsers, or empty string */
  bqfmt: string
  /** Answer format for browsers, or empty string */
  bafmt: string
  /** Template ordinal */
  ord: number
  /** Deck override.
   * @default null
   */
  did: number | null
}

export interface ModelField {
  /** The name of the field */
  name: string
  /** Editor font family */
  font: string
  /** Editor text size */
  size: number
  /** Field ordinal */
  ord: number
  /** Right-to-left text */
  rtl: boolean
  /** Retain last value when adding a new note */
  sticky: boolean
  /** Unused */
  media: []
}
