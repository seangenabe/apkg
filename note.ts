import { getUnixTime } from "./util"
import nanoid from "nanoid"
import { createHash } from "crypto"

export function createNote<T extends string[]>({
  id,
  mid,
  mod,
  usn = -1,
  tags = "",
  values
}: {
  id?: number
  mid: number
  mod?: number
  usn?: number
  tags?: string
  values: T
}): Note {
  const unixNow = getUnixTime()
  return {
    id: id || unixNow,
    guid: nanoid(),
    flags: 0,
    data: "",
    mid,
    mod: mod || id || unixNow,
    usn,
    tags,
    flds: values.join("\x1f"),
    sfld: values[0],
    csum: checksumField(values[0])
  }
}

/**
 * Used for note.csum. Get the checksum of the specified field. For notes, must be the value of the first field
 * @param field
 */
export function checksumField(field: string) {
  const sha1sum = createHash("sha1")
    .update(field)
    .digest()
  return sha1sum.subarray(0, 4).readUInt32BE(0)
}

export interface MutableNote {
  mid: number // models JSON
  mod: number
  usn: number
  tags: string
  flds: string
  sfld: string
  csum: number
}

export interface Note extends MutableNote {
  id: number
  guid: string
  flags: number
  data: string
}
