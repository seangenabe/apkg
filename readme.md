# apkg

Anki SRS apkg reader/writer

## Usage

```typescript
import { pipeline } from "@improved/node/stream"
import { ZipFile } from "yazl"
import { Apkg } from "@seangenabe/apkg"

async function run() {
  const apkg = new Apkg()

  // modify apkg here

  const files = apkg.save()

  const zip = new ZipFile()
  for (let file of files) {
    zip.addReadStream(Readable.from(file.read(), { objectMode: false }), file.name)
  }
  zip.end()
  await pipeline(
    zip.outputStream,
    createWriteStream("output.apkg")
  )
}
```

### class Apkg

Represents an Anki collection file.

#### #constructor()

#### #media: ReadableFile[]

Media files

#### #col: Collection

Collection object.

The `conf`, `models`, `decks`, `dconf`, and `tags` properties need not be assigned as they are assigned with objects from the `Apkg` instance.

#### #conf: CollectionConfiguration

`conf`

#### #models: Model[]

Note types

#### #decks: Deck[]

Decks

#### #dconf: DeckConfiguration[]

Deck options

#### #notes: Note[]

Notes

#### #cards: Card[]

#### #loadFromDb(db: SqlJs.Database)

Loads a collection from an Sql.js database.

#### #saveToDb(db: SqlJs.Database)

Saves the collection to an Sql.js database.

#### #save(): IterableIterator<ReadableFile>

Saves Apkg data to a series of files that can be saved into a `.apkg` zip file.

A `ReadableFile` is an object with a property `name` for the file name and a `read(): AsyncIterableIterator<Uint8Array>` function that reads from a file (Compatible with a node.js Readable stream).

#### #load(files: IterableIterator<ReadableFile>): Promise<void>

Loads Apkg data from a series of files.

## Abstraction of file series and streams

An abstraction of series of files and file streams is chosen so as to leave the choice of choosing a zip-handling implementation to the consumer. Likewise, the stream abstraction is to make the package environment-agnostic (node.js / browser).
