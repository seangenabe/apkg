import { test, run } from "t0"
import initSqlJs from "sql.js"
import { initDbSql } from "./init-db"
import { Apkg } from "."

test("init db sql", async () => {
  const SQL = await initSqlJs()
  const db = new SQL.Database()
  db.run(initDbSql())
})

test("init", async () => {
  new Apkg()
})

test("save", async () => {
  const apkg = new Apkg()
  for (let file of apkg.save()) {
    for await (let _ of file.read()) {
    }
  }
})

run()
