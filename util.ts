import { SqlJs } from "sql.js/module"
import Cache from "quick-lru"

export function getUnixTime() {
  return Math.floor(new Date().getTime() / 1000)
}

/**
 * Adds colons to object keys suitable as SQLite parameters
 * @param obj
 */
export function addColons(obj: SqlJs.ParamsObject) {
  const newObj: SqlJs.ParamsObject = {}
  for (let [key, value] of Object.entries(obj)) {
    newObj[`:${key}`] = value
  }
  return newObj
}

export function fromSqliteRow(row: SqlJs.QueryResults) {
  const result: Record<string, SqlJs.ValueType> = {}
  let i = 0
  for (let value of row.values) {
    result[row.columns[i]] = value
    i++
  }
}

export function getOrCreate<K extends {}, V>(
  map: MapLike<K, V>,
  key: K,
  fn: (key: K) => V
): V {
  if (map.has(key)) {
    return map.get(key)!
  }
  const value = fn(key)
  map.set(key, value)
  return value
}

interface MapLike<K extends {}, V> {
  has(key: K): boolean
  get(key: K): V | undefined
  set(key: K, value: V): this
}
